from django.urls import path
from api import views

urlpatterns = [
    path('values/', views.data_list),
    path('values/<int:pk>/', views.data_detail),
    path('form/<int:pk>/', views.form_data),
    path('form/<int:pk>/id/<int:person_id>/', views.person_form_data),
    path('qid/<int:question_id>/', views.question_form_data),
    path('question/<int:pk>/', views.form_question),
    path('choices/<int:pk>/', views.question_choices),
    path('type/<int:pk>/id/<int:person_id>/', views.person_data),
    path('type/<int:pk>/id/<int:person_id>/template', views.index),
    path('test', views.trypost),
    path('detail/<int:form_id>', views.detail),
]
