from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from postform.models import *
from api.serializers import *
from formentry.submodals.formGenerator import formValue, questions, QuestionChoice, headings
from personal.submodels.person import Personal
from personal.models import *

import pandas as pd

@csrf_exempt
def data_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        my_data = formEntries.objects.all()
        serializer = FormEntrySerializer(my_data, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = FormEntrySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def data_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = formEntries.objects.get(pk=pk)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = FormEntrySerializer(form_entry)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = FormEntrySerializer(form_entry, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        form_entry.delete()
        return HttpResponse(status=204)

@csrf_exempt
def form_data(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = formEntries.objects.filter(form=pk)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = FormEntrySerializer(form_entry, many = True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = FormEntrySerializer(form_entry, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        form_entry.delete()
        return HttpResponse(status=204)

@csrf_exempt
def person_form_data(request, pk, person_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = formEntries.objects.filter(form=pk, member=person_id)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = FormEntrySerializer(form_entry, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = FormEntrySerializer(form_entry, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        form_entry.delete()
        return HttpResponse(status=204)

@csrf_exempt
def question_form_data(request, question_id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = formEntries.objects.filter( formfield=question_id)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = FormEntrySerializer(form_entry, many=True)
        return JsonResponse(serializer.data, safe=False)

@csrf_exempt
def form_question(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = questions.objects.get(pk=pk)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = FormQuestionSerializer(form_entry)
        return JsonResponse(serializer.data)


@csrf_exempt
def question_choices(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        form_entry = QuestionChoice.objects.filter(questionID=pk)
    except formEntries.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = QuestionChoiceSerializer(form_entry, many=True)
        return JsonResponse(serializer.data, safe=False)

import personal.submodels.family as fmly

@csrf_exempt
def person_data(request, pk, person_id):
    """
    pk = Type of Request: Person, Family or house
    Retrieve, update or delete a code snippet.
    """
    if pk==1:
        try:
            person = Personal.objects.filter(pk=person_id) #.all() #
        except Personal.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = PersonalSerializer(person, many=True)
            return JsonResponse({'data':serializer.data}, safe=False)

    elif pk==2:
        try:
            person = fmly.Family.objects.filter(pk=person_id)
        except fmly.Family.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = FamilySerializer(person, many=True)
            return JsonResponse(serializer.data, safe=False)

    elif pk==3:
        try:
            person = fmly.House.objects.filter(pk=person_id)
        except fmly.Family.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = HouseSerializer(person, many=True)
            return JsonResponse(serializer.data, safe=False)

    elif pk==4:
        try:
            person = fmly.Relation.objects.filter(family=person_id)
        except fmly.Relation.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = RelationSerializer(person, many=True)
            return JsonResponse(serializer.data, safe=False)

    elif pk==5:
        try:
            person = fmly.GeoCode.objects.filter(pk=person_id)
        except fmly.GeoCode.DoesNotExist:
            return HttpResponse(status=404)

        if request.method == 'GET':
            serializer = GeoSerializer(person, many=True)
            return JsonResponse(serializer.data, safe=False)

    else:
        return HttpResponse(status=404)

# Debug Mode
def index(request,pk,person_id):
    #2 template = loader.get_template('polls/index.html')
    context = {

    }
    return render(request, 'api/data.html', context)

@csrf_exempt
def trypost(request):
    if request.method == 'POST':
        data = request.POST['ids']
        data = eval(data)
        try:
            person = Personal.objects.filter(pk__in=data)
        except Personal.DoesNotExist:
            return HttpResponse(status=404)
        serializer = PersonalSerializer(person, many=True)
        return JsonResponse({'data':serializer.data}, safe=False)
        # return JsonResponse(request.POST, safe=False)
    else:
        # print(request)
        return JsonResponse(request.method, safe=False)



import json

def detail(request, form_id):
    myform = formValue.objects.get(pk=form_id)


    form_headings = headings.objects.filter(formID = form_id) #.values_list('id',flat=True)
    for eachform in form_headings:
        eachform.count = len(questions.objects.filter(tableID = eachform.id))
    form_questions = questions.objects.filter(tableID__in = form_headings.values_list('id', flat=True))
    columns = list(form_questions.values_list('id', flat=True))

    all_values = list(formEntries.objects.filter(form_id = form_id).values_list('id','formfield','answers','member'))
    unique_member_ids = formEntries.objects.filter(form_id = form_id).values_list('member', flat=True).distinct()
    df_values = pd.DataFrame(all_values)
    df_values.columns = ['id','formfield','answers','member']
    df_values = df_values.drop_duplicates(subset = ['member','formfield'], keep = 'first')
    df_values = df_values.pivot(index='member', columns='formfield', values='answers')
    my_values = df_values.fillna('-')

    x=1000
    y = []
    if myform.formType == 3:
        while (len(my_values.index) - x) > -1000:
            mycollection = Personal.objects.filter(id__in = list(my_values.index) [(x-1000):(x-1)])
            for item in mycollection:
                y.append((item.id,str(item)))
            x = x + 999


    elif myform.formType == 2:
        while (len(my_values.index) - x) > -1000:
            mycollection = Family.objects.filter(id__in = list(my_values.index) [(x-1000):(x-1)])
            for item in mycollection:
                y.append((item.id,str(item)))
            x = x + 999
    elif myform.formType == 1:
        while (len(my_values.index) - x) > -1000:
            mycollection = House.objects.filter(id__in = list(my_values.index) [(x-1000):(x-1)])
            for item in mycollection:
                y.append((item.id,str(item)))
            x = x + 999
    f = [len(y) ,len(my_values.index)]
    my_values['name'] = pd.Series(dict(y))
    # my_values = my_values.iloc[my_values['name'].dropna().index,:]

    context = {
    'table':form_headings, 'questions': form_questions,
    # 'values':(my_values.values), 'uniques':list(unique_member_ids), 'formid':form_id,  'member_ids': my_values.index, 'json_data': my_values.to_json(),
    'columns_id': columns,
    'json_transpose': my_values.T.to_json(),
    'all_choices': dict(list(QuestionChoice.objects.all().values_list('id','choiceDescription'))),
    'questions_with_choices': list(questions.objects.filter(answerType='sc').values_list('id', flat=True)),
    'questions_with_yes_no': list(questions.objects.filter(answerType='yn').values_list('id', flat=True)),
    'thenames': mycollection, 'formType': myform.formType,

    }
    return render(request, 'api/anup.html', context)
