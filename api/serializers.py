from rest_framework import serializers
from postform.models import *


class FormEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = formEntries
        fields = ('id','formfield','answers','member','form')

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return formEntries.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        # instance.title = validated_data.get('title', instance.title)
        # instance.code = validated_data.get('code', instance.code)
        # instance.linenos = validated_data.get('linenos', instance.linenos)
        # instance.language = validated_data.get('language', instance.language)
        # instance.style = validated_data.get('style', instance.style)
        # instance.save()
        return instance

import formentry.submodals.formGenerator as fG

class FormQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = fG.questions
        fields = ('id', 'answerType', 'question', 'tableID_id', 'marks', 'mandatory', 'unanswering', 'description', 'weight', 'choicequestion')

    def create(self, validated_data):
        return fG.questions.create(validated_data)

    def update(self, instance, validated_data):
        return instance

class QuestionChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = fG.QuestionChoice
        fields = ('id', 'questionID_id', 'choiceDescription', 'weight', 'choiceMarks')

    def create(self, validated_data):
        return fG.QuestionChoice.create(validated_data)

    def update(self, instance, validated_data):
        return instance

from personal.submodels.person import Personal

class PersonalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Personal
        fields = ('id','fname','mname','lname','pub_date','gened','creator', 'male_female','gender')

    def create(self, validated_data):
        return Personal.create(validated_data)

    def update(self, instance, validated_data):
        return instance

import personal.submodels.family as fmly

class FamilySerializer(serializers.ModelSerializer):
    class Meta:
        model = fmly.Family
        fields = ('id','person_head_id', 'gened', 'myhouse_id')

    def create(self, validated_data):
        return fmly.Family.create(validated_data)

    def update(self, instance, validated_data):
        return instance

class HouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = fmly.House
        fields = ('coordinates_id', 'owner_id')

    def create(self, validated_data):
        return fmly.House.create(validated_data)

    def update(self, instance, validated_data):
        return instance

class RelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = fmly.Relation
        fields = ('family_id', 'person2_id', 'relntype')

    def create(self, validated_data):
        return fmly.Relation.create(validated_data)

    def update(self, instance, validated_data):
        return instance

class GeoSerializer(serializers.ModelSerializer):
    class Meta:
        model = fmly.GeoCode
        fields = ('latitude','longitude')

    def create(self, validated_data):
        return fmly.GeoCode.create(validated_data)

    def update(self, instance, validated_data):
        return instance
