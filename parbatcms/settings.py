"""
Django settings for parbatcms project.
haha
Generated by 'django-admin startproject' using Django 1.11.1.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=8rpf-f6-%o(v%*e==#ay(@dbqelbll4%=skd$3t^+@e%@3l$2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['parbat.anup.pro.np','localhost','103.69.124.248', '103.69.125.102']


# Application definition

INSTALLED_APPS = [
    # 'jet.dashboard',
    # 'jet',
    'widget_tweaks',
    'api.apps.ApiConfig',
    'postform.apps.PostformConfig',
    'polls.apps.PollsConfig',
    'personal.apps.PersonalConfig',
    'formentry.apps.FormentryConfig',
    'users.apps.UsersConfig',
    'photouploads.apps.PhotouploadsConfig',
    # 'dajaxice',
    # 'dajax',
    # 'mydajax.apps.MydajaxConfig',
    # 'grappelli',
    'rest_framework',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'parbatcms.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
                # 'django.template.context_processors.request',
            ],
        },
    },
]

WSGI_APPLICATION = 'parbatcms.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {

#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'parbatcms',
#         'USER': 'root',
#         'PASSWORD': 'Adhik@r!',
#         'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
#         'PORT': '3306',
# 	'CHARSET':'utf8',
# }

   'default': {
       'ENGINE': 'django.db.backends.sqlite3',
       'NAME': os.path.join(BASE_DIR, 'db1.sqlite3'),
   },
    # 'primary': {
    #     'NAME': os.path.join(BASE_DIR, 'db2.sqlite3'),
    #     'ENGINE': 'django.db.backends.sqlite3',
    # },
    # 'replica1': {
    #     'NAME': os.path.join(BASE_DIR, 'db3.sqlite3'),
    #     'ENGINE': 'django.db.backends.sqlite3',
    # },
    # 'replica2': {
    #     'NAME': os.path.join(BASE_DIR, 'db4.sqlite3'),
    #     'ENGINE': 'django.db.backends.sqlite3',
    # },
}
# DATABASE_ROUTERS = [
#     # 'path.to.AuthRouter'
#     os.path.join(BASE_DIR, 'authrouter.py'),
#     # 'path.to.PrimaryReplicaRouter'
#     os.path.join(BASE_DIR, 'replica.py'),
# ]

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

JET_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'

STATIC_ROOT = os.path.join(BASE_DIR, "static/")

# DAJAXICE_MEDIA_PREFIX="dajaxice"
#
# TEMPLATE_LOADERS = (
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
# )

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}
