from django.apps import AppConfig


class PhotouploadsConfig(AppConfig):
    name = 'photouploads'
