# Generated by Django 2.0.4 on 2018-05-26 05:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formentry', '0011_questions_choicequestion'),
    ]

    operations = [
        migrations.AddField(
            model_name='formvalue',
            name='jsonText',
            field=models.TextField(default='', verbose_name='Json Text'),
            preserve_default=False,
        ),
    ]
